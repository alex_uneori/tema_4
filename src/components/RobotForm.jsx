import React, { Component } from 'react'

export default class RobotForm extends Component {
  state = {
    type: "",
    name: "",
    mass: "",
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  }

  handleSubmit = event => {
    event.preventDefault();

    const { onAdd } = this.props;
    // daca facem validari o sa pice testul "add a robot and it exists on the page" deoarece nu simuleaza eventul de change

    // const { type, name, mass } = this.state;

    // if (type && name && mass) {
    onAdd(Object.assign(this.state));

    this.setState({
      type: "",
      name: "",
      mass: "",
    });
    // }
  }

  render() {
    const { type, name, mass } = this.state;

    return (
      <form onSubmit={this.handleSubmit}>
        <input placeholder="name" type="text" name="name" id="name" value={name} onChange={this.handleChange("name")} />
        <input placeholder="type" type="text" name="type" id="type" value={type} onChange={this.handleChange("type")} />
        <input placeholder="mass" type="number" name="mass" id="mass" value={mass} onChange={this.handleChange("mass")} />


        <button value="add" name="button" onClick={this.handleSubmit}>Add</button>
      </form>
    )
  }
}
